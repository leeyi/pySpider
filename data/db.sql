create database `py_spider` default charset utf8;

GRANT ALL PRIVILEGES ON `py_spider`.* TO 'admin'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
flush privileges;

CREATE TABLE `s_truck` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plate_number_all` varchar(40) DEFAULT NULL COMMENT '车牌号：冀e7g495',
  `load` varchar(20) DEFAULT NULL COMMENT '车载重量体积',
  `from_city` varchar(20) DEFAULT NULL COMMENT '发车城市',
  `to_city` varchar(20) DEFAULT NULL COMMENT '期望去向城市',
  `start_date` char(10) DEFAULT NULL COMMENT '发车时间',
  `car_type` varchar(10) DEFAULT NULL COMMENT '车辆车型： 高栏车 平板车 低栏车 等',
  `car_length` varchar(10) DEFAULT NULL COMMENT '车长',
  `contact_name` varchar(20) DEFAULT NULL COMMENT '联系人名称',
  `contact_idcard` varchar(20) DEFAULT NULL COMMENT '联系人身份证',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系人电话',
  `url` varchar(200) DEFAULT NULL COMMENT '数据源URL',
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=34187 DEFAULT CHARSET=utf8 COMMENT='车辆信息表';

CREATE TABLE `s_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_city` varchar(20) DEFAULT NULL COMMENT '发车城市',
  `to_city` varchar(20) DEFAULT NULL COMMENT '期望去向城市',
  `start_date` char(10) DEFAULT NULL COMMENT '发车时间',
  `goods_info` varchar(200) DEFAULT NULL COMMENT '货物信息',
  `freight` varchar(20) DEFAULT NULL COMMENT '运费',
  `volume` varchar(20) DEFAULT NULL COMMENT '体积',
  `weight` varchar(20) DEFAULT NULL COMMENT '重量',
  `car_type` varchar(10) DEFAULT NULL COMMENT '需求车辆车型： 高栏车 平板车 低栏车 等',
  `car_length` varchar(10) DEFAULT NULL COMMENT '需求车长',
  `contact_name` varchar(20) DEFAULT NULL COMMENT '联系人名称',
  `contact_idcard` varchar(20) DEFAULT NULL COMMENT '联系人身份证',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT '联系人电话',
  `url` varchar(200) DEFAULT NULL COMMENT '数据源URL',
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品信息表';
