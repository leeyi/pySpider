# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymysql

# class PyspiderPipeline(object) :
#     def process_item(self, item, spider) :
#         return item

def dbHandle() :
    conn = pymysql.connect(
        host = '127.0.0.1',
        db = 'elibdb',
        user = 'admin',
        passwd = '123456',
        charset = 'utf8',
        cursorclass = pymysql.cursors.DictCursor,
        use_unicode = False,
    )
    return conn

class DriverPipeline(object) :
    def process_item(self, item, spider) :
        dbObject = dbHandle()
        cursor = dbObject.cursor()
        sql = "REPLACE INTO `s_driver` (`plate_number_all`, `load`, `from_city`, `to_city`, `start_date`, `car_type`, `car_length`, `contact_name`, `contact_idcard`, `contact_phone`, `url`, `remark`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
        values = (item['plate_number_all'], item['load'], item['from_city'], item['to_city'], item['start_date'], item['car_type'], item['car_length'], item['contact_name'], item['contact_idcard'], item['contact_phone'], item['url'], item['remark'])
        try :
            cursor.execute(sql, values)
            dbObject.commit()
        except :
            dbObject.rollback()

        return item