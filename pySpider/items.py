# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PyspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class TruckItem(scrapy.Item):
    # define the fields for your item here like:
    plate_number_all = scrapy.Field() # '车牌号：冀e7g495',
    load             = scrapy.Field() # '车载重量体积',
    from_city        = scrapy.Field() # '发车城市',
    to_city          = scrapy.Field() # '期望去向城市',
    start_date       = scrapy.Field() # 发车时间',
    car_type         = scrapy.Field() # ''车辆车型： 高栏车 平板车 低栏车 等',
    car_length       = scrapy.Field() # '车长',
    contact_name     = scrapy.Field() # '联系人名称',
    contact_idcard   = scrapy.Field() # '联系人身份证',
    contact_phone    = scrapy.Field() # '联系人电话',
    url              = scrapy.Field() # 数据源URL
    remark           = scrapy.Field() # 备注
