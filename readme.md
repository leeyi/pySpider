# 资料参考

http://scrapy-chs.readthedocs.io/zh_CN/0.24/intro/tutorial.html

# 安装 scrapy

```
// 使用Python3
ln -s /usr/local/bin/python3 /usr/local/bin/python

pip3 install Scrapy
pip3 install pymysql
// 模拟登陆需要
pip3 install requests
pip3 install bs4
```

# 项目初始化
```

scrapy startproject pySpider
cd pySpider
```

## 编写一个爬虫

```
scrapy genspider babasuper http://www.babasuper.com

scrapy edit babasuper

    start_urls = [
        'http://www.babasuper.com/port/trucksource.shtml'
    ]
```

### 抓取

```
scrapy crawl babasuper
```

### 运行
```
# cd pySpider/pySpider/spiders
# 要先cd 到爬虫所在的目录（暂时不知道不这样的话，是否可行？）
scrapy runspider babasuper.py

scrapy parse http://www.babasuper.com -c parse_item
```

### version
```
scrapy version -v
```

### 在Shell中尝试Selector选择器

```
scrapy shell <url>

scrapy shell http://www.babasuper.com/port/trucksource.shtml

scrapy shell http://www.gapp.gov.cn/zongshu/serviceSearchListbve.shtml
```

当shell载入后，您将得到一个包含response数据的本地 response 变量。输入 response.body 将输出response的包体， 输出 response.headers 可以看到response的包头。

更为重要的是，当输入 response.selector 时， 您将获取到一个可以用于查询返回数据的selector(选择器)， 以及映射到 response.selector.xpath() 、 response.selector.css() 的 快捷方法(shortcut): response.xpath() 和 response.css() 。


```
class BabasuperSpider(scrapy.Spider):
    name = "babasuper"
    allowed_domains = ["http://www.babasuper.com"]
    start_urls = [
        'http://http://www.babasuper.com/port/trucksource.shtml/'
    ]

    def parse(self, response):
        pass
```

### 读取scrapy配置文件setting.py中的方法
```
from scrapy.utils.project import get_project_settings
settings = get_project_settings()
```

## 爬虫 publisher

```
ln -s /Library/Frameworks/Python.framework/Versions/3.6/bin/scrapy /usr/local/bin/scrapy

scrapy genspider publisher http://www.gapp.gov.cn/zongshu/serviceSearchListbve.shtml
```
### 运行
```
cd pySpider/spiders
# 要先cd 到爬虫所在的目录（暂时不知道不这样的话，是否可行？）
scrapy runspider publisher.py
```

